package com.enndava.innovation.controllers;

import com.enndava.innovation.models.Person;
import com.enndava.innovation.models.PersonImageRegister;
import com.enndava.innovation.services.PeopleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/people")
public class PeopleController {

    @Autowired
    private PeopleService peopleService;

    @RequestMapping("/getPerson")
    public ResponseEntity<Person> getPerson(String personName) {
        Person result = peopleService.getPerson(personName);
        if(result != null)
            return new ResponseEntity<>(result, HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping("/getPersonTrackData")
    public ArrayList<String> getPersonTrackData(String personName) {
        return peopleService.getPerson(personName).getTrackHistory();
    }

    @RequestMapping("/getAllPeople")
    public ResponseEntity<List<Person>> getAllPeople() {
        return new ResponseEntity<>(peopleService.getAllPeople(), HttpStatus.OK);
    }
    
    @RequestMapping("/getAllPeopleMap")
    public ResponseEntity<Map<String, Person>> getAllPeopleMap() {
        return new ResponseEntity<>(peopleService.getAllPeopleMap(), HttpStatus.OK);
    }
    
    
    @PostMapping("/savePerson")
    public ResponseEntity savePerson(@RequestBody Person person) {
        return new ResponseEntity(peopleService.savePerson(person));
    }

//    @RequestMapping(value = "/savePerson", method = RequestMethod.POST)
//    public ResponseEntity savePerson(@RequestBody PersonImageRegister personImageRegister) {
//        return new ResponseEntity(peopleService.savePerson(personImageRegister.getBase64()));
//    }

    @RequestMapping("/addPhoto")
    public ResponseEntity addPhoto(String personName, byte[] personImage) {
        return new ResponseEntity(peopleService.addPhoto(personName, personImage));
    }
}
