import React, {Component} from 'react';
import {Link} from "react-router-dom";

class ModalBox extends Component {
  constructor(props) {
    super(props);

    this.doShowModal = this.doShowModal.bind(this);
    this.doHandleClick = this.doHandleClick.bind(this);
  }

  doShowModal(){
    this.props.showModal(false);
  }

  doHandleClick(e) {
    if(this.refs.node.contains(e.target)) return;

    this.props.showModal(false);
  }

  componentWillMount() {
    document.addEventListener('mousedown', this.doHandleClick, false);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.doHandleClick, false);
  }

  render() {
    let type = this.props.toggle + 's',
      allData = this.props.allData;

    return (
      <div className="modal-box-wrapper">
        <div ref="node" className="modal-box">
          <div className="modal-box-name">
            <h2>Add New <span>{this.props.toggle}</span></h2>

            <button className="close" onClick={this.doShowModal}>
              <i className="fa fa-times"></i>
            </button>
          </div>

          <div className="modal-box-search">
            <div className="input-group">
              <i className="fa fa-search"></i>
              <input type="text" title="search" placeholder={"Search for " + type} ref="searchValue"/>
            </div>
          </div>

          <div className="modal-box-list">
            <ul>
              {
                allData[type].map((item, k) => {
                  return <li key={k}>
                    <Link to={'/'+this.props.toggle+'/'+item.name} onClick={this.doShowModal}>{item.name}</Link>
                  </li>
                })
              }
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default ModalBox;
