package com.enndava.innovation.controllers;

import java.time.LocalTime;

import com.enndava.innovation.models.Event;
import com.enndava.innovation.services.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.enndava.innovation.constants.EventLevels.DANGER;
import static com.enndava.innovation.constants.EventTopics.SECURITY_BREACH;

@RestController
public class PrivateVideoController {

    LocalTime vladTracker = LocalTime.now();
    LocalTime georgeTracker = LocalTime.now();

    @Autowired
    private EventService eventService;

        @GetMapping(value = "/sendPrivateRoomVideoEvent")
        public void getPrivateRoomVideoEvent(String personName) {
            if((personName.equals("Vlad Aiftimie") /*&& Duration.between(LocalTime.now(), vladTracker).toMinutes() > 0.3*/)) {
                vladTracker = LocalTime.now();
                Event event = new Event();
                event.setEventLocation("Private Room");
                event.setEventName("Security breach");
                event.setEventDescription("Unauthorized personel found in Private Room");
                event.setEventTime(System.currentTimeMillis());
                event.setTriggerPerson(personName);
                event.setEventType(DANGER);
                eventService.triggerEvent(event, SECURITY_BREACH);
            } else if((personName.equals("George Birsanuc") /*&& Duration.between(LocalTime.now(), georgeTracker).toMinutes() > 0.3*/)) {
                georgeTracker = LocalTime.now();
                Event event = new Event();
                event.setEventLocation("Private Room");
                event.setEventName("Security breach");
                event.setEventDescription("Unauthorized personel found in Private Room");
                event.setEventTime(System.currentTimeMillis());
                event.setTriggerPerson(personName);
                event.setEventType(DANGER);
                eventService.triggerEvent(event, SECURITY_BREACH);
            }
    }
}
