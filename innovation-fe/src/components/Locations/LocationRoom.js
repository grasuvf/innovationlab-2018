import React, {Component} from 'react';
import Borders from './../Elements/Borders';

import AWS from 'aws-sdk';
//import videojs from 'video.js';
import * as VHS from 'videojs-contrib-hls';
import * as shaka from 'shaka-player';

class LocationRoom extends Component {

  displayVideoFeed(){
    let streamName = 'Innovation-vid';
    let options = {
      accessKeyId: 'AKIAJ242BG7Q4Y33C7TQ',
      secretAccessKey: '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',
      sessionToken: undefined,
      region: 'eu-west-1',
      endpoint: undefined
    };
    let kinesisVideo = new AWS.KinesisVideo(options);
    let kinesisVideoArchivedContent = new AWS.KinesisVideoArchivedMedia(options);

    kinesisVideo.getDataEndpoint({
      StreamName: streamName,
      APIName: "GET_HLS_STREAMING_SESSION_URL"
    }, function(err, response) {
      if (err) { return console.error(err); }
      kinesisVideoArchivedContent.endpoint = new AWS.Endpoint(response.DataEndpoint);
      kinesisVideoArchivedContent.getHLSStreamingSessionURL({
        StreamName: streamName,
        PlaybackMode: 'LIVE',
        HLSFragmentSelector: {
          FragmentSelectorType: 'SERVER_TIMESTAMP',
          TimestampRange: undefined
        },
        DiscontinuityMode: 'ALWAYS'
      }, function(err, response) {
        if (err) { return console.error(err); }

        const player = new shaka.Player(document.getElementById('videojs'));

        player.load(response.HLSStreamingSessionURL).then(function() {
            console.log('Starting playback');
        });
        console.log('Set player source');
      });
    });
  }

  componentDidMount() {
    this.displayVideoFeed();
  }

  componentWillUnmount() {
    // let player = videojs('videojs');
    // player.dispose();
  }

  render() {
    let allData = this.props.allData,
      thisCity = this.props.city,
      thisFloor = this.props.floor,
      thisRoom = this.props.room,
      floors = allData.locations.filter(obj => {
        return obj.name.toLowerCase() === thisCity.toLowerCase()
      })[0].floors,
      floor = floors.filter(obj => {
        return obj.name.toLowerCase() === thisFloor.toLowerCase()
      })[0],
      room = floor.rooms.filter(obj => {
        return obj.name.toLowerCase() === thisRoom.toLowerCase()
      })[0];

    return (
      <div className="location-room">
        <div className="room-map">
          <div id="playerContainer" className="playerContainer">
            <video id="videojs" className="player" autoPlay></video>
          </div>

          <Borders top="true" right="true" bottom="true" left="true"/>
        </div>
      </div>
    );
  }
}

export default LocationRoom;
