package com.enndava.innovation.constants;

public class EventLevels {
    public static final String WARNING = "Warning";
    public static final String NOTICE = "Notice";
    public static final String DANGER = "Danger";
}
