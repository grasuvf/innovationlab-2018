package com.enndava.innovation.aws.rekognition;


import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.rekognition.AmazonRekognition;
import com.amazonaws.services.rekognition.AmazonRekognitionClientBuilder;
import com.amazonaws.services.rekognition.model.CreateCollectionRequest;
import com.amazonaws.services.rekognition.model.CreateCollectionResult;
import com.amazonaws.services.rekognition.model.CreateStreamProcessorRequest;
import com.amazonaws.services.rekognition.model.CreateStreamProcessorResult;
import com.amazonaws.services.rekognition.model.DeleteStreamProcessorRequest;
import com.amazonaws.services.rekognition.model.DescribeStreamProcessorRequest;
import com.amazonaws.services.rekognition.model.FaceRecord;
import com.amazonaws.services.rekognition.model.FaceSearchSettings;
import com.amazonaws.services.rekognition.model.Image;
import com.amazonaws.services.rekognition.model.IndexFacesRequest;
import com.amazonaws.services.rekognition.model.IndexFacesResult;
import com.amazonaws.services.rekognition.model.KinesisDataStream;
import com.amazonaws.services.rekognition.model.KinesisVideoStream;
import com.amazonaws.services.rekognition.model.ListStreamProcessorsRequest;
import com.amazonaws.services.rekognition.model.StartStreamProcessorRequest;
import com.amazonaws.services.rekognition.model.StopStreamProcessorRequest;
import com.amazonaws.services.rekognition.model.StreamProcessorInput;
import com.amazonaws.services.rekognition.model.StreamProcessorOutput;
import com.amazonaws.services.rekognition.model.StreamProcessorSettings;

import java.util.List;

public class VideoAnalysisService {

  public static void main(String[] args) {
    VideoAnalysisService service = new VideoAnalysisService();
//    service.deleteStreamProcessor();
//    service.describeStreamProcessor();
    Regions[] regions = Regions.values();
    for (int i = 0; i < regions.length; ++i) {
      try {
        AmazonRekognition amazonRekognition = AmazonRekognitionClientBuilder.standard()
            .withCredentials(new DefaultAWSCredentialsProviderChain())
            .withRegion(regions[i])
            .build();
        service.describeStreamProcessor(amazonRekognition);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  private static final AmazonRekognition amazonRekognition = AmazonRekognitionClientBuilder.standard()
      .withCredentials(new DefaultAWSCredentialsProviderChain())
      .withRegion(Regions.EU_WEST_1)
      .build();
  private static final String REKOGNITION_PROCESSOR_NAME = "rekognition-processor";
  private static final String VIDEO_STREAM_ARN = "arn:aws:kinesisvideo:eu-west-1:471266675257:stream/AmazonRekognition-example-video/1526683246473";
  private static final String DATA_STREAM_ARN = "arn:aws:kinesis:eu-west-1:471266675257:stream/AmazonRekognition-example-data";
  private static final String ROLE_ARN = "arn:aws:iam::471266675257:role/Rekognition";
  private static final String COLLECTION_ID = "face-collection";

  private int IMAGE_ID = 0;

  public void createFacesCollection() {
    CreateCollectionRequest request = new CreateCollectionRequest().withCollectionId(COLLECTION_ID);
    CreateCollectionResult result = amazonRekognition.createCollection(request);
  }

  public void indexFace(Image image) {
    IndexFacesRequest indexFacesRequest = new IndexFacesRequest()
        .withImage(image)
        .withCollectionId(COLLECTION_ID)
        .withExternalImageId(String.valueOf(++IMAGE_ID))
        .withDetectionAttributes("ALL");

    IndexFacesResult indexFacesResult = amazonRekognition.indexFaces(indexFacesRequest);

    System.out.println("Image indexed");
    List<FaceRecord> faceRecords = indexFacesResult.getFaceRecords();
    for (FaceRecord faceRecord : faceRecords) {
      System.out.println("Face detected: Faceid is " +
          faceRecord.getFace().getFaceId());
    }
  }

  public void createStreamProcessor() {
    CreateStreamProcessorRequest request = new CreateStreamProcessorRequest()
        .withInput(
            new StreamProcessorInput().withKinesisVideoStream(
                new KinesisVideoStream().withArn(VIDEO_STREAM_ARN)
            ))
        .withOutput(
            new StreamProcessorOutput().withKinesisDataStream(
                new KinesisDataStream().withArn(DATA_STREAM_ARN)
            ))
        .withSettings(
            new StreamProcessorSettings().withFaceSearch(
                new FaceSearchSettings()
                    .withFaceMatchThreshold(80F)
                    .withCollectionId(COLLECTION_ID)
            ))
        .withName(REKOGNITION_PROCESSOR_NAME)
        .withRoleArn(ROLE_ARN);

    CreateStreamProcessorResult result = amazonRekognition.createStreamProcessor(request);
  }

  public void startStreamProcessor() {
    amazonRekognition.startStreamProcessor(new StartStreamProcessorRequest().withName(REKOGNITION_PROCESSOR_NAME));
  }

  public void stopStreamProcessor() {
    System.out.println(amazonRekognition.stopStreamProcessor(new StopStreamProcessorRequest().withName(REKOGNITION_PROCESSOR_NAME)).toString());
  }

  public void describeStreamProcessor(AmazonRekognition amazonRekognition) {
    System.out.println(amazonRekognition.listStreamProcessors(new ListStreamProcessorsRequest()).toString());
    System.out.println(amazonRekognition.describeStreamProcessor(new DescribeStreamProcessorRequest().withName(REKOGNITION_PROCESSOR_NAME)).toString());
  }

  public void deleteStreamProcessor() {
    System.out.println(amazonRekognition.deleteStreamProcessor(new DeleteStreamProcessorRequest().withName(REKOGNITION_PROCESSOR_NAME)).toString());
  }
}
