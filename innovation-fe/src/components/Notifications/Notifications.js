import React, {Component} from 'react';
import {Link} from "react-router-dom";
import Borders from './../Elements/Borders';

class Notifications extends Component {
  constructor(props) {
    super(props);
  }

  eventInProgress(time) {
    let now = new Date(),
      notificationTime = new Date(time),
      differenceMinutes = Math.round((now - notificationTime) / 60000);

    return differenceMinutes <= 5; // 5 minutes
  }

  orderByDate(arr) {
    return arr.slice().sort(function (a, b) {
      return a.date > b.date ? -1 : 1;
    });
  }

  render() {
    let allData = this.props.allData,
      max = this.props.max ? this.props.max : 5,
      thisType = this.props.type,
      thisCity = this.props.city ? this.props.city : null,
      allNotifications = allData.notifications,
      notifications = this.orderByDate(allNotifications.filter(obj => {
        if (thisCity) {
          return obj.type.toLowerCase() === thisType.toLowerCase() && obj.location.city.toLowerCase() === thisCity.toLowerCase()
        } else {
          return obj.type.toLowerCase() === thisType.toLowerCase();
        }
      }));

    return (
      <div className={"notifications " + thisType}>
        <div className="notification-type clearfix">
          <h2>{thisType === "danger" ? "Security" : "Notices"}</h2>
          <span className="count">{notifications.length}</span>
          <Borders top="false" right="false" bottom="true" left="false"/>
        </div>

        <div className="notification-list">
          {
            notifications.slice(0, max).map((notification, k) => {
              return <div key={k} className="notification-item">
                <Link to={
                  "/location/" + notification.location.city +
                  "/" + notification.location.floor +
                  "/" + notification.location.room
                }>
                  <h3 className="title">{notification.title}</h3>
                  <p className="location">
                    {
                      !thisCity
                        ? <span>
                          {notification.location.city}, {notification.location.floor}
                          </span>
                        : <span>{notification.location.floor}</span>
                    }
                  </p>
                  {
                    this.eventInProgress(notification.date)
                      ? <span className="active">NOW</span>
                      : <span className="date">{notification.date}</span>
                  }
                </Link>
              </div>
            })
          }
        </div>

        <Borders top="true" right="true" bottom="true" left="true"/>
      </div>
    );
  }
}

export default Notifications;
