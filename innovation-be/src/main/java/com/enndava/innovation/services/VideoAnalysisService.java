package com.enndava.innovation.services;


import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.kinesisvideo.model.GetDataEndpointRequest;
import com.amazonaws.services.rekognition.AmazonRekognition;
import com.amazonaws.services.rekognition.AmazonRekognitionClientBuilder;
import com.amazonaws.services.rekognition.model.*;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VideoAnalysisService {

    private static final AmazonRekognition amazonRekognition = AmazonRekognitionClientBuilder.standard()
            .withCredentials(new DefaultAWSCredentialsProviderChain())
            .withRegion(Regions.EU_WEST_1)
            .build();
    /*
    public static final String REKOGNITION_PROCESSOR_NAME = "rekognition-processor";
    public static final String VIDEO_STREAM_ARN = "arn:aws:kinesisvideo:eu-west-1:471266675257:stream/AmazonRekognition-video-stream/1541093263818";
    public static final String DATA_STREAM_ARN = "arn:aws:kinesis:eu-west-1:471266675257:stream/AmazonRekognition-data-stream";
    public static final String ROLE_ARN = "arn:aws:iam::471266675257:role/Rekognition";
    public static final String COLLECTION_ID = "face-collection";
*/
    private static final String REKOGNITION_PROCESSOR_NAME = "rekognition-processor";
    private static final String VIDEO_STREAM_ARN = "arn:aws:kinesisvideo:eu-west-1:471266675257:stream/Innovation-vid/1542042374837";
    private static final String DATA_STREAM_ARN = "arn:aws:kinesis:eu-west-1:471266675257:stream/Innovation-data";
    private static final String ROLE_ARN = "arn:aws:iam::471266675257:role/Rekognition";
    public static final String COLLECTION_ID = "face-collection";

    private int IMAGE_ID = 0;

    public void createFacesCollection() {
        CreateCollectionRequest request = new CreateCollectionRequest().withCollectionId(COLLECTION_ID);
        CreateCollectionResult result = amazonRekognition.createCollection(request);
        System.out.println(result);
    }

    public void listFaces() {
        System.out.println(amazonRekognition.listFaces(new ListFacesRequest().withCollectionId(COLLECTION_ID)));
    }

    public void indexFace(Image image) {
        IndexFacesRequest indexFacesRequest = new IndexFacesRequest()
                .withImage(image)
                .withCollectionId(COLLECTION_ID)
                .withExternalImageId(String.valueOf(++IMAGE_ID))
                .withDetectionAttributes("ALL");

        IndexFacesResult indexFacesResult = amazonRekognition.indexFaces(indexFacesRequest);

        System.out.println("Image indexed");
        List<FaceRecord> faceRecords = indexFacesResult.getFaceRecords();
        for (FaceRecord faceRecord : faceRecords) {
            System.out.println("Face detected: Faceid is " +
                    faceRecord.getFace().getFaceId());
        }
    }

    public void indexFaceWithPersonName(Image image, String name) {
        IndexFacesRequest indexFacesRequest = new IndexFacesRequest()
                .withImage(image)
                .withCollectionId(COLLECTION_ID)
                .withExternalImageId(name)
                .withDetectionAttributes("ALL");

        IndexFacesResult indexFacesResult = amazonRekognition.indexFaces(indexFacesRequest);

        System.out.println("Image indexed");
        List<FaceRecord> faceRecords = indexFacesResult.getFaceRecords();
        for (FaceRecord faceRecord : faceRecords) {
            System.out.println("Face detected: Faceid is " +
                    faceRecord.getFace().getFaceId());
        }
    }

//    public void indexFaceWithPersonName(Image image, String name) {
//        IndexFacesRequest indexFacesRequest = new IndexFacesRequest()
//                .withImage(image)
//                .withCollectionId(COLLECTION_ID)
//                .withExternalImageId(name)
//                .withDetectionAttributes("ALL");
//
//        IndexFacesResult indexFacesResult = amazonRekognition.indexFaces(indexFacesRequest);
//
//        System.out.println("Image indexed");
//        List<FaceRecord> faceRecords = indexFacesResult.getFaceRecords();
//        for (FaceRecord faceRecord : faceRecords) {
//            System.out.println("Face detected: Faceid is " +
//                    faceRecord.getFace().getFaceId());
//        }
//    }

    public void createStreamProcessor() {
        CreateStreamProcessorRequest request = new CreateStreamProcessorRequest()
                .withInput(
                        new StreamProcessorInput().withKinesisVideoStream(
                                new KinesisVideoStream().withArn(VIDEO_STREAM_ARN)
                        ))
                .withOutput(
                        new StreamProcessorOutput().withKinesisDataStream(
                                new KinesisDataStream().withArn(DATA_STREAM_ARN)
                        ))
                .withSettings(
                        new StreamProcessorSettings().withFaceSearch(
                                new FaceSearchSettings()
                                        .withFaceMatchThreshold(80F)
                                        .withCollectionId(COLLECTION_ID)
                        ))
                .withName(REKOGNITION_PROCESSOR_NAME)
                .withRoleArn(ROLE_ARN);

        CreateStreamProcessorResult result = amazonRekognition.createStreamProcessor(request);
    }

    public void startStreamProcessor() {
        amazonRekognition.startStreamProcessor(new StartStreamProcessorRequest().withName(REKOGNITION_PROCESSOR_NAME));
    }

    public void describeStreamProcessor() {
        DescribeStreamProcessorResult result = amazonRekognition
                .describeStreamProcessor(new DescribeStreamProcessorRequest().withName(REKOGNITION_PROCESSOR_NAME));

        System.out.println(result.toString());
    }

    public void stopStreamProcessor() {
        amazonRekognition.stopStreamProcessor(new StopStreamProcessorRequest().withName(REKOGNITION_PROCESSOR_NAME));
    }
}
