package com.enndava.innovation.controllers;

public class GetAccessResponse {
    private String personName;
    private Boolean authorized;

    public GetAccessResponse(String personName, Boolean authorized) {
        this.personName = personName;
        this.authorized = authorized;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public Boolean getAuthorized() {
        return authorized;
    }

    public void setAuthorized(Boolean authorized) {
        this.authorized = authorized;
    }
}
