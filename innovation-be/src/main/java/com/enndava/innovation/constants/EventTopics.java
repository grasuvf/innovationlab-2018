package com.enndava.innovation.constants;

public class EventTopics {
    public static final String HAZARD = "hazard";
    public static final String SECURITY_BREACH = "security-breach";
    public static final String SECURITY_BREACH_WARNING = "security-breach-warning";
    public static final String ELECTRICITY_WASTE = "electricity-waste";
    public static final String WATER_WASTE = "water-waste";
    public static final String NEW_PERSON_REGISTERED = "new-person-registered";
}
