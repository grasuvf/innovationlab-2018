package com.enndava.innovation.controllers;

import com.amazonaws.services.rekognition.model.Image;
import com.enndava.innovation.aws.rekognition.AwsRekognitionExample;
import com.enndava.innovation.constants.EventLevels;
import com.enndava.innovation.models.Authorization;
import com.enndava.innovation.models.Event;
import com.enndava.innovation.models.PersonImageRegister;
import com.enndava.innovation.services.AccessService;
import com.enndava.innovation.services.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import static com.enndava.innovation.constants.EventTopics.HAZARD;
import static com.enndava.innovation.constants.EventTopics.SECURITY_BREACH;

@RestController
public class AccessController {

    @Autowired
    private AccessService accessService;

    @Autowired
    private EventService eventService;

    @Autowired
    private AwsRekognitionExample awsRekognitionExample;

    @RequestMapping("/getAccess")
    public ResponseEntity<Boolean> getAccess(String personName, String roomName) {
        return new ResponseEntity<>(accessService.getAccess(personName, roomName), HttpStatus.OK);
    }

    @RequestMapping("/getAccessByImage")
    public ResponseEntity<GetAccessResponse> getAccess(@RequestBody GetAccessRequest getAccessRequest) {
        return new ResponseEntity<GetAccessResponse>(
                accessService.getAccess(getAccessRequest.personImage, getAccessRequest.roomName), HttpStatus.OK);
    }

    @RequestMapping(value = "/getAccessByImagePublicRoom", method = RequestMethod.POST)
    public ResponseEntity<Authorization> getAccessPublicRoom(@RequestBody PersonImageRegister personImageRegister) {
        return new ResponseEntity<>(accessService.getAccessByBase64String(personImageRegister.getBase64(), "Public Room"), HttpStatus.OK);
    }

    @RequestMapping(value = "/getAccessByImagePrivateRoom", method = RequestMethod.POST)
    public ResponseEntity<Authorization> getAccessPrivateRoom(@RequestBody PersonImageRegister personImageRegister) {
        return new ResponseEntity<>(accessService.getAccessByBase64String(personImageRegister.getBase64(), "Private Room"), HttpStatus.OK);
    }

    @RequestMapping(value = "/sendProducerImage", method = RequestMethod.POST)
    public ResponseEntity getImage(@RequestBody byte[] byteImage) {


        Image image = AccessService.getAWSImageFromBytes(byteImage);
        try {
            awsRekognitionExample.main(image);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ResponseEntity(HttpStatus.CREATED);
    }

    @RequestMapping(value = "/sendFireEvent", method = RequestMethod.GET)
    public void getFireEvent() {
        Event event = new Event();
        event.setEventDescription("A fire has been spotted on the ground floor!");
        event.setEventLocation("Carmen Silvae");
        event.setEventName("Fire alarm");
        event.setEventTime(System.currentTimeMillis());
        event.setTriggerPerson(null);
        event.setEventType(HAZARD);
        eventService.triggerEvent(event, HAZARD);
        System.out.println("FIREEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEe");
    }
    
    @GetMapping(value = "/sendUnknownEvent")
    public void sendUnknownEvent() {
        String roomName = "Public Room";
        Event event = new Event();
        event.setEventLocation(roomName);
        event.setEventName("Unauthorized Security Check");
        event.setEventDescription("An unknown personnel has requested access in the room: " + roomName);
        event.setEventTime(System.currentTimeMillis());
        event.setEventType(EventLevels.DANGER);
        eventService.triggerEvent(event, SECURITY_BREACH);
    }
}
