import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router,Route,Switch} from 'react-router-dom';
import * as dateFns from 'date-fns';

import localJSON from './local.json';
import Header from './components/Elements/Header';
import Footer from './components/Elements/Footer';
import ModalBox from './components/Elements/ModalBox';
import Home from './components/home';
import Location from './components/Locations/Location';

import webstomp from 'webstomp-client';
import SockJS from 'sockjs-client';

import registerServiceWorker from './registerServiceWorker';
import 'font-awesome/css/font-awesome.min.css';
import './assets/css/index.css';

class App extends Component {
  constructor(props){
    super(props);

    this.state = {
      showModal: false,
      allData: localJSON
    };

    this.doShowModal = this.doShowModal.bind(this);
    this.connect = this.connect.bind(this);
  }

  doShowModal(state) {
    this.setState({
      showModal: state
    });
  }

  connect() {
    let socket = new SockJS('http://192.168.220.206:8080/envision');
    let stompClient = webstomp.over(socket);
    stompClient.connect({}, (frame) => {

      stompClient.subscribe('/topic/new-person-registered', (notice) => {

        notice = JSON.parse(JSON.parse(JSON.stringify(notice)).body);

        this.addNotification(notice);
      });

      stompClient.subscribe('/topic/security-breach-warning', (notice) => {

        notice = JSON.parse(JSON.parse(JSON.stringify(notice)).body);

        this.addNotification(notice);
      });

      stompClient.subscribe('/topic/security-breach', (notice) => {

        notice = JSON.parse(JSON.parse(JSON.stringify(notice)).body);

        this.addNotification(notice);
      });
    });
  }

  addNotification = (notification) => {
    const { allData : { notifications } } = this.state;
    const title = `${notification.eventName} ${notification.triggerPerson ? `- ${notification.triggerPerson}` : ''}`;
    const newNotification = {
      type: notification.eventType === 'Notice' ? 'warning' : 'danger',
      title,
      location: {
        city: 'Iasi E5',
        floor: 'First Floor',
        room: 'room1',
      },
      date: dateFns.format(new Date(notification.eventTime), 'MM.DD.YYYY H:mm:ss'),
    };

    notifications.push(newNotification);
    this.setState({
      allData: {
        ...this.state.allData,
        notifications,
      }
    });
  }

  componentDidMount() {
    this.connect();
  }

  render() {
    return (
      <Router>
        <div>
          <Header showModal={this.doShowModal}/>

          <Switch>
            <Route
              exact
              path="/location/:city/:floor?/:room?"
              render={(props) => <Location {...props} allData={this.state.allData} />}
            />
            <Route render={(props) => <Home {...props} allData={this.state.allData} />} />
          </Switch>

          { this.state.showModal
            ? <ModalBox toggle={this.state.showModal} showModal={this.doShowModal} allData={this.state.allData} />
            : null
          }

          <Footer/>
        </div>
      </Router>
    );
  }
}

ReactDOM.render(
  <App/>,
  document.getElementById('root')
);

registerServiceWorker();
