import React, {Component} from 'react';
import Heading from './../Elements/Heading';
import Notifications from './../Notifications/Notifications';
import LocationBuilding from './LocationBuilding';
import LocationFloor from './LocationFloor';
import LocationRoom from './LocationRoom';

class Location extends Component {
  render() {
    let allData = this.props.allData,
      heading = 'General Access Admin',
      thisCity = this.props.match.params.city ? this.props.match.params.city : null,
      thisFloor = this.props.match.params.floor ? this.props.match.params.floor : null,
      thisRoom = this.props.match.params.room ? this.props.match.params.room : null;

    let buildingView = !thisFloor && !thisRoom,
      floorView = thisFloor && !thisRoom,
      roomView = thisFloor && thisRoom;

    if (floorView) {
      heading = thisFloor;
    }
    if (roomView) {
      heading = thisFloor + ' / ' + thisRoom;
    }

    return (
      <div className="grid-container">
        <div className="grid-row">
          <div className="grid-col-10">
            <Heading title={heading} location={thisCity}/>
          </div>
        </div>

        { buildingView ?
          <div className="grid-row">
            <div className="grid-col-3 same-height">
              <Notifications type="danger" city={thisCity} max="5" allData={allData}/>
            </div>
            <div className="grid-col-4">
              <div className="location-map">
                <LocationBuilding city={thisCity} allData={allData}/>
              </div>
            </div>
            <div className="grid-col-3 same-height">
              <Notifications type="warning" city={thisCity} max="5" allData={allData}/>
            </div>
          </div>
          :
          <div className="grid-row">
            <div className="grid-col-3">
              <Notifications type="danger" city={thisCity} max="3" allData={allData}/>
              <Notifications type="warning" city={thisCity} max="3" allData={allData}/>
            </div>
            <div className="grid-col-7">
              <div className="location-map">
                {
                  floorView
                    ? <LocationFloor city={thisCity} floor={thisFloor} allData={allData}/>
                    : null
                }
                {
                  roomView
                    ? <LocationRoom city={thisCity} floor={thisFloor} room={thisRoom} allData={allData}/>
                    : null
                }
              </div>
            </div>
          </div>
        }
      </div>
    );
  }
}

export default Location;
