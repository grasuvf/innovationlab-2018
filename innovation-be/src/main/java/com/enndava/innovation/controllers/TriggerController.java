package com.enndava.innovation.controllers;

import com.enndava.innovation.models.Event;
import com.enndava.innovation.services.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.enndava.innovation.constants.EventTopics.HAZARD;

@RestController
public class TriggerController {

    @Autowired
    EventService eventService;

    @RequestMapping("/trigger")
    public void trigger() {
        Event event = new Event();
        event.setEventName("Fire hazard");
        event.setEventDescription("A fire has been lit");
        event.setEventLocation("Presentation room");
        event.setEventTime(System.currentTimeMillis());
        event.setTriggerPerson("Vlad Aiftimie");
        eventService.triggerEvent(event, HAZARD);
    }
}
