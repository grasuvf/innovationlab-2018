import React, {Component} from 'react';

class Footer extends Component {
  render() {
    return (
      <footer className="footer">
        <div className="grid-container">
          <div className="grid-row">
            <div className="grid-col-5">
              <span>ENvision Innovation Lab Team. All rights reserved.</span>
            </div>
            <div className="grid-col-5 text-right">
              <span>Powered by Endava</span>
            </div>
          </div>
        </div>
      </footer>
    );
  }
}

export default Footer;
