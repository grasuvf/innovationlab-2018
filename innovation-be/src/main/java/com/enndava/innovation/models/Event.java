package com.enndava.innovation.models;

import lombok.Data;

@Data
public class Event {
    private String eventName;
    private String eventDescription;
    private long eventTime;
    private String eventLocation;
    private String triggerPerson;
    private String eventType;
}
