package com.enndava.innovation.constants;

public class SecurityLevels {
    public static final String PUBLIC_AREA = "Public Area";
    public static final String PRIVATE_AREA = "Private Area";
    public static final String STAFF_ONLY_AREA = "Staff Only Area";
    public static final String AUTHORIZED_PERSONNEL_ONLY_AREA = "Authorized Personnel Only Area";
}
