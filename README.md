# README #

# To do (UI)
 - porting sidebar menu to React
 - create edit page for Employees
    - add/edit/delete employee
    - possibility to quickly upload an employee photo from mobile (for later due)
 - create edit page for Locations
    - add nr of floors
    - upload floor plan image
    - draw points over floor plan to create rooms
    - configure each rooms with data like: nr of video feeds etc
 - search functionality
    - look for someone in sidebar menu search -> display building, floor and room where employee was found (some css animation effects blah blah)

# Ideas we have so far - in a very particular order #
## VERY IMPORTANT ##

TEST VIDEO REKOGNITION WITH PHOTO


- optymize AWS consumption -> build a graph with cameras -> use adjacent to track


## LESS IMPORTANT ##
- layer on video stream
- staff + unknown = secure enough, pair and track 2 roles (an unknown person to be constantly escorted)
- smart office integration (where is electricity/water wasted tracker)
- heat map layer over all rooms
- track unknown people + saving data (video)
- alerts for clean desk policy


# DONE #
- track unknown people + saving data (image)
- video tracking
- design the building/rooms (+ grad de securitate per incapere)
- make aws video rekognition work
- hazard detection (fire -> on the spot | SEND A SMS AUTOMATICALY TO THE AUDIENCE WHEN THE FIRE HAZARD OCCURS)
- face recognition -> Photo -> for entry access (DONE)
                 +
- mobile app for registering + photo + alerts
- real-time notification system: Kafka
- assign roles: staff, guest, unknown, etc.
- import building layout


# Back-end How to configure the environment (Kafka) #

Download Apache Kafka binaries from https://www.apache.org/dyn/closer.cgi?path=/kafka/1.1.0/kafka_2.11-1.1.0.tgz

__Start the zookeeper server:__
   Go into your kafka folder/bin/windows and run 
      __zookeeper-server-start.bat ../../config/zookeeper.properties__

__Start the kafka server:__
   Go into your kafka folder/bin/windows and run 
      __kafka-server-start.bat ../../config/server.properties__
      
__Documentation__ for Apache Kafka can be found here: https://kafka.apache.org/documentation/


# API DOCUMENTATION #

/getAccess
   INPUT: String personName, String roomName
   OUTPUT: boolean
   
/getAccessByImage
   INPUT: byte[] personImage, String roomName
   OUTPUT: boolean
   
   

/people/getPerson
   INPUT: String personName
   OUTPUT: Person
   
/people/getAllPeople
   INPUT: -
   OUTPUT: List<Person>
   
/people/savePerson
   INPUT: Person
   OUTPUT: Status Code
   
/people/addPhoto
   INPUT: String personName, byte[] personImage
   OUTPUT: Status Code



/rooms/getRoom
   INPUT: String roomName
   OUTPUT: Room
   
/rooms/getAllRooms
   INPUT: -
   OUTPUT: List<Room>
   
/room/saveRoom
   INPUT: Room
   OUTPUT: Status Code
   
   
   
Person
   String name
   List<byte[]> images
   int age
   String department
   String role
   String[] roomsAccess

Room
   String roomName
   String[] videoStreams
   String securityLevel
   

