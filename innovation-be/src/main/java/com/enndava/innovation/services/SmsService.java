package com.enndava.innovation.services;

import com.nexmo.client.NexmoClient;
import com.nexmo.client.NexmoClientException;
import com.nexmo.client.auth.AuthMethod;
import com.nexmo.client.auth.TokenAuthMethod;
import com.nexmo.client.sms.SmsSubmissionResult;
import com.nexmo.client.sms.messages.TextMessage;

import java.io.IOException;

public class SmsService {

    private static final String API_KEY = "c01a833c";
    private static final String SECRET_KEY = "uf1lhvXtwrsA3F3y";

    private static AuthMethod authMethod = new TokenAuthMethod(API_KEY, SECRET_KEY);
    private static NexmoClient nexmoClient = new NexmoClient(authMethod);

    public static void main(String... args) throws IOException, NexmoClientException {

        String[] numbers = {"40755491753"};
        sendSMS(numbers, "Es acasa\n\n");
    }

    public static void sendSMS(String[] numbers, String message) {
        for (String n : numbers) {
            try {
                SmsSubmissionResult[] responses;
                responses = nexmoClient.getSmsClient().submitMessage(new TextMessage(
                        "ENvision",
                        n,
                        message));

                for (SmsSubmissionResult response : responses) {
                    System.out.println(response);
                }
            } catch (IOException | NexmoClientException e) {
                e.printStackTrace();
            }
        }
    }
}
