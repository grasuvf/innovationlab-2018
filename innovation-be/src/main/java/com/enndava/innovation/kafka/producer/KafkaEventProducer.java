package com.enndava.innovation.kafka.producer;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

public class KafkaEventProducer {
    private int messageNo = 1;
    private KafkaProducer producer;
    private boolean isAsync;
    private boolean productionMode;

    private static final String KAFKA_SERVER_URL = "localhost";
    private static final int KAFKA_SERVER_PORT = 9092;
    private static final String CLIENT_ID = "KafkaEventProducer";

    public KafkaEventProducer(Boolean isAsync, boolean productionMode) {
        this.productionMode = productionMode;
        Properties properties = new Properties();
        properties.put("bootstrap.servers", KAFKA_SERVER_URL + ":" + KAFKA_SERVER_PORT);
        properties.put("client.id", CLIENT_ID);
        properties.put("key.serializer", "org.apache.kafka.common.serialization.IntegerSerializer");
        properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        producer = new KafkaProducer(properties);
        this.isAsync = isAsync;
    }

    public void triggerEvent(String topic, String messageStr) {
        long startTime = System.currentTimeMillis();
        if (this.isAsync) {
            this.triggerEventAsync(this.productionMode, topic, messageStr, startTime);
            this.messageNo++;
        } else {
            this.triggerEventSync(this.productionMode, topic, messageStr);
            this.messageNo++;
        }
    }

    private void triggerEventAsync(boolean isProduction, String topic, String messageStr, long startTime) {
        if (isProduction) {
            producer.send(new ProducerRecord<>(topic, this.messageNo, messageStr), new DemoCallBack(startTime, this.messageNo, messageStr));
        } else {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Send message? y/n");
            String i;
            try {
                i = br.readLine();
                if (i.equals("y")) {
                    producer.send(new ProducerRecord<>(topic, this.messageNo, messageStr), new DemoCallBack(startTime, this.messageNo, messageStr));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void triggerEventSync(boolean isProduction, String topic, String messageStr) {
        try {
            if(isProduction) {
                producer.send(new ProducerRecord<>(topic, messageNo, messageStr)).get();
            } else {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Send message? y/n");
                String i;
                try {
                    i = br.readLine();
                    if (i.equals("y")) {
                        producer.send(new ProducerRecord<>(topic, messageNo, messageStr)).get();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch(InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }
}
