import React, {Component} from 'react';
import {Link} from "react-router-dom";
import Borders from './../Elements/Borders';

class Header extends Component {
  constructor(props) {
    super(props);

    this.doShowModal = this.doShowModal.bind(this);
  }

  doShowModal(e){
    let toggle = e.currentTarget.getAttribute('value');

    this.props.showModal(toggle);
  }

  render() {
    return (
      <header className="header">
        <div className="header-container clearfix">
          <div className="header-logo">
            <Link to="/">ENvision</Link>
          </div>

          <div className="header-search">
            <div className="input-group">
              <i className="fa fa-search"></i>
              <input type="text" title="search" placeholder="Search for locations, employees, warning types..." ref="searchValue"/>
            </div>
            <div className="search-dropdown">

            </div>
          </div>

          <div className="header-settings">
            <button className="open-locations" onClick={this.doShowModal} value="location">
              <i className="fa fa-map-marker"></i>
            </button>
            <button className="open-employees" onClick={this.doShowModal} value="employee">
              <i className="fa fa-user-plus"></i>
            </button>
            <button className="open-admin">
              <i className="fa fa-cog"></i>
            </button>
            <button className="open-logout">
              <i className="fa fa-sign-out"></i>
            </button>
          </div>

          <Borders top="false" right="false" bottom="true" left="false"/>
        </div>
      </header>
    );
  }
}

export default Header;
