import React, {Component} from 'react';
import Borders from './../Elements/Borders';

class LocationFloor extends Component {
  render() {
    // let allData = this.props.allData,
    //   thisCity = this.props.city,
    //   thisFloor = this.props.floor,
    //   floors = allData.locations.filter(obj => {
    //     return obj.name.toLowerCase() === thisCity.toLowerCase()
    //   })[0].floors;
      // floor = floors.filter(obj => {
      //   return obj.name.toLowerCase() === thisFloor.toLowerCase()
      // })[0];

    return (
      <div className="location-floor">
        <div className="floor-map">
          <img src="https://i.imgur.com/KOXOBiN.gif"/>

          <Borders top="true" right="true" bottom="true" left="true"/>
        </div>
      </div>
    );
  }
}

export default LocationFloor;
