import React, { Fragment } from 'react';
import { StyleSheet, Text, View, TextInput } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import { Card, CheckBox, Avatar, Button } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';

const ACCESS_LEVELS = [
  {label: 'Public', value: 'public'},
  {label: 'Restricted', value: 'restricted'},
  {label: 'Guest', value: 'guest'},
];

const ROOMS = [
  {id: 'room1', label: 'Room 1'},
  {id: 'room2', label: 'Room 2'},
  {id: 'room3', label: 'Room 3'},
  {id: 'room4', label: 'Room 4'},
];

const Form = (props) => (
  <ScrollView>
    <Card 
      title="Employee information" 
      containerStyle={{marginBottom: 20, marginHorizontal: 0}}
    >
      <View style={{display: 'flex', alignItems: 'center'}}>
        <Avatar
          xlarge
          rounded
          source={{uri: "http://www.thestoryof.org/wp-content/uploads/2016/09/profile-placeholder.jpg"}}
          containerStyle={{marginBottom: 20}}
        />
      </View>
      <TextInput
        style={styles.textInput}
        placeholder="First Name"
        onChangeText={value => props.handleFieldChange('firstName', value)}
      />
      <TextInput
        style={styles.textInput}
        placeholder="Last Name"
        onChangeText={value => props.handleFieldChange('lastName', value)}
      />
    </Card>
    <Card title="Access control" containerStyle={{marginBottom: 20, marginHorizontal: 0}}>
      <Text style={styles.label}>Access Level</Text>
      <RNPickerSelect
        placeholder={{
            label: 'Select access level',
            value: 'none',
        }}
        items={ACCESS_LEVELS}
        onValueChange={value => props.handleFieldChange('accessLevel', value)}
        value={props.accessLevel}
        style={{ ...pickerSelectStyles }}
      />
      {
        props.accessLevel !== '' && props.accessLevel !== 'public' &&
        <Fragment>
          <Text style={styles.label}>Accessible rooms</Text>
          {
            ROOMS.map(room => 
              <CheckBox
                key={room.id}
                title={room.label}
                checked={props.rooms.includes(room.id)}
                onPress={() => props.handleRooms(room.id)}
              />  
            )
          }
        </Fragment>
      }
    </Card>
    <Button
      title='Save employee'
      backgroundColor='#dc4227'
      onPress={props.saveEmployee}  
    />

  </ScrollView>
);

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      firstName: '',
      lastName: '',
      accessLevel: '',
      rooms: [],
    };
  }

  handleFieldChange = (field, value) => {
    this.setState({
      [field]: value
    });
  }

  handleRooms = (roomId) => {
    let { rooms } = this.state;
    if (!rooms.includes(roomId)) {
      rooms.push(roomId);
    } else {
      const roomIndex = rooms.findIndex(roomId);
      rooms.splice(roomIndex, 1);
    }

    this.setState({rooms});
  }

  saveEmployee = () => {
    console.log(this.state);
  }

  render() {
    return (
      <View style={styles.container}>
        <Form
          accessLevel={this.state.accessLevel}
          rooms={this.state.rooms}
          handleFieldChange={this.handleFieldChange}
          handleRooms={this.handleRooms}
          saveEmployee={this.saveEmployee}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    paddingTop: 50,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 50
  },
  textInput: {
    borderWidth: 0.5,
    borderColor: '#d6d7da',
    height: 50,
    marginBottom: 20,
    paddingLeft: 12
  },
  label: {
    marginBottom: 12,
  }
});

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
      fontSize: 16,
      paddingTop: 12,
      paddingHorizontal: 12,
      paddingBottom: 12,
      borderWidth: 1,
      borderColor: '#d6d7da',
      backgroundColor: 'white',
      color: 'black',
      marginBottom: 20
  },
});
