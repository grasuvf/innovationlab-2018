package com.enndava.innovation.configuration;

import com.enndava.innovation.services.AccessService;
import com.enndava.innovation.services.EventService;
import com.enndava.innovation.services.PeopleService;
import com.enndava.innovation.services.RoomsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

import java.io.IOException;

@Configuration
@EnableWebSocketMessageBroker
public class ApplicationConfiguration implements WebSocketMessageBrokerConfigurer {

    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        config.enableSimpleBroker("/topic");
        config.setApplicationDestinationPrefixes("/app");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/envision")
                .setAllowedOrigins("*")
                .withSockJS();
    }

    @Bean
    public AccessService accessService() {
        return new AccessService();
    }

    @Bean
    public PeopleService peopleService() throws IOException {
        return new PeopleService();
    }

    @Bean
    public RoomsService roomsService() {
        return new RoomsService();
    }

    @Bean
    @Autowired
    public EventService hazardEventService(SimpMessageSendingOperations messagingTemplate) {
        return new EventService(messagingTemplate);
    }
}
