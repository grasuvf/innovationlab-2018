import React, { Component, Fragment } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TextInput,
} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import RNPickerSelect from 'react-native-picker-select';
import { CheckBox, Button, Avatar } from 'react-native-elements';
import { RNCamera } from 'react-native-camera';
import axios from 'axios';

const IP = '192.168.220.190';

const ACCESS_LEVELS = [
  {label: 'Public', value: 'public'},
  {label: 'Restricted', value: 'restricted'},
  {label: 'Guest', value: 'guest'},
];

const ROOMS = [
  {id: 'Public Room', label: 'Public Room'},
  {id: 'Secure Room', label: 'Secure Room'},
  {id: 'Server Room', label: 'Server Room'},
];

const Form = (props) => (
  <ScrollView>
    <View style={styles.pageTitleWrapper}>
      <Text style={styles.pageTitle}>Add new</Text>  
    </View>
    <View style={styles.padded}>
      <View style={{display: 'flex', alignItems: 'center'}}>
        <Avatar
          xlarge
          rounded
          source={{
            isStatic: true,
            uri: `data:image/jpeg;base64,${props.avatar}`,
          }}
          containerStyle={{marginVertical: 20}}
        />
      </View>
      <Text style={styles.sectionTitle}>Employee Information</Text>
      <TextInput
        style={styles.textInput}
        placeholder="First Name"
        onChangeText={value => props.handleFieldChange('firstName', value)}
        placeholderTextColor={'#fff'}
      />
      <TextInput
        style={styles.textInput}
        placeholder="Last Name"
        onChangeText={value => props.handleFieldChange('lastName', value)}
        placeholderTextColor={'#fff'}
      />
      <Text style={styles.sectionTitle}>Access Control</Text>
      <RNPickerSelect
        placeholder={{
            label: 'Access Level',
            value: 'none',
        }}
        items={ACCESS_LEVELS}
        onValueChange={value => props.handleFieldChange('accessLevel', value)}
        value={props.accessLevel}
        style={{ ...pickerSelectStyles }}
        placeholderTextColor={'#fff'}
      />
      {
        props.accessLevel !== '' && props.accessLevel !== 'public' &&
        <Fragment>
          {
            ROOMS.map(room => 
              <CheckBox
                key={room.id}
                title={room.label}
                checked={props.rooms.includes(room.id)}
                checkedColor='#175FF4'
                onPress={() => props.handleRooms(room.id)}
                containerStyle={{
                  backgroundColor: '#1A1B21',
                  borderWidth: 0
                }}
                textStyle={{
                  fontFamily: 'Oswald',
                  fontSize: 18,
                  color: '#fff',
                }}
              />  
            )
          }
        </Fragment>
      }
      <Button
        title='Add employee'
        onPress={props.saveEmployee}
        buttonStyle={styles.button}
        textStyle={{
          color: '#fff',
          fontFamily: 'Oswald',
          fontSize: 18,
        }}
      />
      <Button
        title='Go back'
        onPress={props.goBack}
        buttonStyle={styles.button}
        textStyle={{
          color: '#fff',
          fontFamily: 'Oswald',
          fontSize: 18,
        }}
      />
    </View>
  </ScrollView>
);

const Viewfinder = ({bindCameraRef, takePicture, cameraMode}) => (
  <View style={styles.viewfinder}>
    <RNCamera
      ref={ref => {
        bindCameraRef(ref);
      }}
      style = {styles.preview}
      type={RNCamera.Constants.Type[cameraMode]}
      flashMode={RNCamera.Constants.FlashMode.off}
      permissionDialogTitle={'Permission to use camera'}
      permissionDialogMessage={'We need your permission to use your camera phone'}
    />
    <View style={{flex: 0, flexDirection: 'row', justifyContent: 'center',}}>
      <Button
        title='Take photo'
        onPress={takePicture}
        buttonStyle={styles.captureButton}
        textStyle={{
          color: '#fff',
          fontFamily: 'Oswald',
          fontSize: 18,
        }}
      />
    </View>
  </View>
);

const AppSelectionScreen = ({selectApp}) => (
  <View style={{
    flex: 1, 
    alignItems: 'center',
    justifyContent: 'center', 
    backgroundColor: '#15161C'
  }}>
    <Button
      title='Register a new employee'
      onPress={() => { selectApp('register') }}
      buttonStyle={styles.captureButton}
      textStyle={{
        color: '#fff',
        fontFamily: 'Oswald',
        fontSize: 18,
      }}
    />
    <Button
      title='Grant access'
      onPress={() => { selectApp('grant') }}
      buttonStyle={styles.captureButton}
      textStyle={{
        color: '#fff',
        fontFamily: 'Oswald',
        fontSize: 18,
      }}
    />
  </View>
);

const FullBackground = ({avatar, info: { isLoading, isAuthorized, personName }}) => {
  const message = isLoading ? 'Authorizing...' : personName;
  const bgColor = isLoading ? '#D99519' : isAuthorized ? '#0B59F8' : '#FF0530';;

  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#15161C'}}>
      <View style={{display: 'flex', alignItems: 'center'}}>
        <Avatar
          xlarge
          rounded
          source={{
            isStatic: true,
            uri: `data:image/jpeg;base64,${avatar}`,
          }}
          containerStyle={{marginVertical: 20}}
        />
      </View>
      <View style={{
        backgroundColor: bgColor,
        paddingVertical: 20, 
        paddingHorizontal: 60
      }}>
        <Text style={{
          textAlign: 'center',
          fontFamily: 'Oswald',
          fontSize: 24,
          color: '#fff',
        }}>
          {message}
        </Text>
        {
          !isLoading && (
            <Text style={{
              textAlign: 'center',
              fontFamily: 'Oswald',
              fontSize: 24,
              color: '#fff',
            }}>
              {`Access ${isAuthorized ? 'granted' : 'denied'}`}
            </Text>
          )
        }
      </View>
    </View>
  );
}

const initialState = {
  picture: {
    isTaken: false,
    data: null,
  },
  firstName: '',
  lastName: '',
  accessLevel: '',
  rooms: [],
  /** SelectedApp: oneOf(['register', 'grant']) */
  selectedApp: '',
  authorize: {
    isAuthorized: false,
    isLoading: false,
    personName: '',
  }
};

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ...initialState
    }
  }

  selectApp = (app) => [
    this.setState({
      selectedApp: app,
    })
  ]

  handleFieldChange = (field, value) => {
    this.setState({
      [field]: value
    });
  }

  handleRooms = (roomId) => {
    let { rooms } = this.state;
    if (!rooms.includes(roomId)) {
      rooms.push(roomId);
    } else {
      const roomIndex = rooms.indexOf(roomId);
      rooms.splice(roomIndex, 1);
    }

    this.setState({rooms});
  }

  saveEmployee = () => {
    const person = {
      name: `${this.state.firstName} ${this.state.lastName}`,
      age: 23,
      images: [
        this.state.picture.data,
      ],
      roomsAccess: this.state.rooms,
    };

    axios({
      method: 'POST',
      url:`http://${IP}:8080/people/savePerson`,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      data: person,
    })
    .then(() => {
      this.resetState();
    });
  }

  resetState = () => {
    this.setState({
      ...initialState
    });
  }

  bindCameraRef = (ref) => {
    this.camera = ref;
  }

  render() {
    const { picture, selectedApp } = this.state;

    if (!selectedApp) {
      return (
        <AppSelectionScreen selectApp={this.selectApp} />
      );
    }
    
    const cameraMode = selectedApp === 'register' ? 'back' : 'front';
    const componentToShow = selectedApp === 'register' ? (
      <View style={styles.container}>
        <Form
          accessLevel={this.state.accessLevel}
          rooms={this.state.rooms}
          handleFieldChange={this.handleFieldChange}
          handleRooms={this.handleRooms}
          saveEmployee={this.saveEmployee}
          goBack={this.resetState}
          avatar={this.state.picture.data}
        />
      </View>
    ) : (
      <FullBackground
        info={this.state.authorize}
        avatar={this.state.picture.data}
      />
    );

    return (
      picture.isTaken ? (
        componentToShow
      ) : (
        <Viewfinder
          bindCameraRef={this.bindCameraRef}
          takePicture={() => this.takePicture(selectedApp)}
          cameraMode={cameraMode}
        />
      )
    )
  }

  takePicture = (selectedApp) => {
    if (this.camera) {
      const options = {
        width: 1024,
        quality: 0.3, 
        base64: true
      };
      
      this.camera.takePictureAsync(options).then(photo => {
        if (selectedApp === 'grant') {
          this.setState({
            picture: {
              isTaken: true,
              data: photo.base64,
            },
            authorize: {
              ...initialState.authorize,
              isLoading: true,
            }
          });
          console.log('start');
          axios({
            method: 'POST',
            url:`http://${IP}:8080/getAccessByImage`,
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            },
            data: {
              personImage: photo.base64,
              roomName: 'Secure Room'
            },
          })
          .then(response => {
            this.setState({
              authorize: {
                isLoading: false,
                isAuthorized: response.data.authorized,
                personName: response.data.personName ? response.data.personName : 'Unknown person',
              }
            });
            setTimeout(() => {
              this.resetState();
            }, 5000);
          }).catch(() => {
            this.setState({
              authorize: {
                isAuthorized: false,
                isLoading: false,
                personName: 'Unknown person',
              }
            });
          });
        } else {
          this.setState({
            picture: {
              isTaken: true,
              data: photo.base64,
            }
          });
        }
      });
    }
  };

  // takePicture = async function() {
  //   if (this.camera) {
  //     const options = {
  //       width: 1024,
  //       quality: 0.3, 
  //       base64: true 
  //     };
  //     this.camera.takePictureAsync(options).then(data => {
        // axios({
        //   method: 'POST',
        //   url:'http://89.238.254.138:8080/people/savePerson',
        //   headers: {
        //     'Accept': 'application/json',
        //     'Content-Type': 'application/json',
        //   },
        //   data: data
        // })
        // .then((response) => {
        //   console.log(response);
        // });
  //       console.log(data);
  //     }).catch(err => console.log(err));
  //   }
  // };
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#15161C',
    paddingBottom: 50,
    flex: 1,
  },
  viewfinder: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#15161C'
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  padded: {
    paddingHorizontal: 24,
  },
  pageTitleWrapper: {
    backgroundColor: '#1A1B21',
    borderBottomWidth: 1,
    borderColor: '#2F3035',
    paddingVertical: 24,
    marginHorizontal: -20
  },
  pageTitle: {
    color: '#fff',
    fontFamily: 'Oswald',
    fontSize: 18,
    textAlign: 'center',
  },
  sectionTitle: {
    color: '#fff',
    fontFamily: 'Oswald',
    fontSize: 24,
    textAlign: 'left',
    marginVertical: 24,
  },
  textInput: {
    borderBottomWidth: 1,
    borderColor: '#2F3035',
    fontFamily: 'Oswald',
    fontSize: 18,
    color: '#fff',
    height: 50,
    marginBottom: 20,
    padding: 12,
  },
  button: {
    backgroundColor:'#175FF4',
    marginTop: 24,
  },
  captureButton: {
    backgroundColor:'#175FF4',
    flex: 0,
    alignSelf: 'center',
    paddingHorizontal: 30,
    margin: 30,
  },
});

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontFamily: 'Oswald',
    fontSize: 18,
    paddingTop: 12,
    paddingHorizontal: 12,
    paddingBottom: 12,
    borderBottomWidth: 1,
    borderColor: '#2F3035',
    backgroundColor: '#15161C',
    color: '#fff',
    marginBottom: 20,
  },
});

AppRegistry.registerComponent('App', () => App);