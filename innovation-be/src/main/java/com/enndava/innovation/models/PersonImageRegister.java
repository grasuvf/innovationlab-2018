package com.enndava.innovation.models;

import lombok.Data;

@Data
public class PersonImageRegister {
    private String width;
    private String height;
    private String uri;
    private String base64;
}