package com.enndava.innovation.services;

import com.enndava.innovation.models.Room;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

import static com.enndava.innovation.constants.SecurityLevels.AUTHORIZED_PERSONNEL_ONLY_AREA;
import static com.enndava.innovation.constants.SecurityLevels.PUBLIC_AREA;

public class RoomsService {

    private List<Room> rooms = new ArrayList<>();

    public RoomsService() {
        Room firstRoom = new Room("Secure Room", new String[] {"firstURL", "secondURL"}, AUTHORIZED_PERSONNEL_ONLY_AREA);
        Room secondRoom = new Room("Public Room", new String[] {"firstURL", "secondURL"}, PUBLIC_AREA);

        rooms.add(firstRoom);
        rooms.add(secondRoom);
    }

    public Room getRoom(String roomName) {
        for(Room room : rooms) {
            if(room.getRoomName().equals(roomName)) {
                return room;
            }
        }
        return null;
    }

    public List<Room> getAllRooms() {
        return this.rooms;
    }

    public HttpStatus saveRoom(Room room) {
        this.rooms.add(room);
        return HttpStatus.OK;
    }
}
