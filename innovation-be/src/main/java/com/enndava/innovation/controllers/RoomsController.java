package com.enndava.innovation.controllers;

import com.enndava.innovation.models.Room;
import com.enndava.innovation.services.RoomsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/rooms")
public class RoomsController {

    @Autowired
    private RoomsService roomsService;

    @RequestMapping("/getRoom")
    public ResponseEntity<Room> getRoom(String roomName) {
        Room result = roomsService.getRoom(roomName);
        if(result != null) {
            return new ResponseEntity<>(result, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping("/getAllRooms")
    public ResponseEntity<List<Room>> getAllRooms() {
        return new ResponseEntity<>(roomsService.getAllRooms(), HttpStatus.OK);
    }

    @RequestMapping("/saveRoom")
    public ResponseEntity saveRoom(Room room) {
        return new ResponseEntity(HttpStatus.OK);
    }
}
