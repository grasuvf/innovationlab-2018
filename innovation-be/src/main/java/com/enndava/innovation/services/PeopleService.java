package com.enndava.innovation.services;

import com.amazonaws.util.IOUtils;
import com.enndava.innovation.models.Event;
import com.enndava.innovation.models.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.time.LocalTime;
import java.util.*;

import static com.enndava.innovation.constants.EventLevels.NOTICE;
import static com.enndava.innovation.constants.EventTopics.NEW_PERSON_REGISTERED;
import static com.enndava.innovation.services.AccessService.getAWSImageFromBytes;

@Service
public class PeopleService {
    private List<Person> people = new ArrayList<>();
    public static Map<String, Person> peopleMap = new HashMap<>();

    @Autowired
    private EventService eventService;

    @Autowired
    private VideoAnalysisService videoAnalysisService;

    public PeopleService() throws IOException {
//        Person vlad = new Person();
//        vlad.setAge(24);
//        vlad.setDepartment("Development");
//        vlad.setName("Vlad Aiftimie");
//        vlad.setRole("Senior Technician");
//        vlad.setRoomsAccess(new String[]{"Public Room"});
//        vlad.setLastTimeTracked(LocalTime.now());
//        ArrayList<String> trackHistory = new ArrayList<>();
//        trackHistory.add(vlad.getName() + " has been added in the tracking system.");
//        vlad.setTrackHistory(trackHistory);
//        InputStream inputStream = new FileInputStream(new File("Vlad Aiftimie.jpg"));
//        vlad.setImages(new ArrayList<byte[]>(){{add(IOUtils.toByteArray(inputStream));}});
//        people.add(vlad);
//        peopleMap.put(vlad.getName().replaceAll(" ", ""), vlad);
//
//        Person catalin = new Person();
//        catalin.setAge(22);
//        catalin.setDepartment("Development");
//        catalin.setName("Catalin Popusoi");
//        catalin.setRole("Senior Technician");
//        catalin.setRoomsAccess(new String[]{"Public Room", "Private Room"});
//        catalin.setLastTimeTracked(LocalTime.now());
//        ArrayList<String> trackHistory2 = new ArrayList<>();
//        trackHistory.add(catalin.getName() + " has been added in the tracking system.");
//        catalin.setTrackHistory(trackHistory2);
//        InputStream inputStream2 = new FileInputStream(new File("Catalin Papusoi.jpg"));
//        catalin.setImages(new ArrayList<byte[]>(){{add(IOUtils.toByteArray(inputStream2));}});
//        people.add(catalin);
//        peopleMap.put(catalin.getName().replaceAll(" ", ""), catalin);
//
//        Person george = new Person();
//        george.setAge(24);
//        george.setDepartment("Development");
//        george.setName("George Birsanuc");
//        george.setRole("Senior Technician");
//        george.setRoomsAccess(new String[]{"Public Room"});
//        george.setLastTimeTracked(LocalTime.now());
//        ArrayList<String> trackHistory3 = new ArrayList<>();
//        trackHistory.add(george.getName() + " has been added in the tracking system.");
//        george.setTrackHistory(trackHistory3);
//        InputStream inputStream3 = new FileInputStream(new File("George Birsanuc.jpg"));
//        george.setImages(new ArrayList<byte[]>(){{add(IOUtils.toByteArray(inputStream3));}});
//        people.add(george);
//        peopleMap.put(george.getName().replaceAll(" ", ""), george);
//
//        Person alex = new Person();
//        alex.setAge(32);
//        alex.setDepartment("Business Analysis");
//        alex.setName("Alexandru Mirza-Lesan");
//        alex.setRole("Senior Business Analyst");
//        alex.setRoomsAccess(new String[]{"Public Room", "Private Room"});
//        alex.setLastTimeTracked(LocalTime.now());
//        ArrayList<String> trackHistory4 = new ArrayList<>();
//        trackHistory.add(alex.getName() + " has been added in the tracking system.");
//        alex.setTrackHistory(trackHistory4);
//        InputStream inputStream4 = new FileInputStream(new File("Alexandru Mirza-Lesan.jpg"));
//        alex.setImages(new ArrayList<byte[]>(){{add(IOUtils.toByteArray(inputStream4));}});
//        people.add(alex);
//        peopleMap.put(alex.getName().replaceAll(" ", ""), alex);
//
//        Person florin = new Person();
//        florin.setAge(26);
//        florin.setDepartment("Development");
//        florin.setName("Florin Grasu");
//        florin.setRole("Engineer");
//        florin.setRoomsAccess(new String[]{"Public Room", "Private Room"});
//        florin.setLastTimeTracked(LocalTime.now());
//        ArrayList<String> trackHistory5 = new ArrayList<>();
//        trackHistory.add(florin.getName() + " has been added in the tracking system.");
//        florin.setTrackHistory(trackHistory5);
//        InputStream inputStream5 = new FileInputStream(new File("Florin Grasu.jpg"));
//        florin.setImages(new ArrayList<byte[]>(){{add(IOUtils.toByteArray(inputStream5));}});
//        people.add(florin);
//        peopleMap.put(florin.getName().replaceAll(" ", ""), florin);
    }

    public Person getPerson(String personName) {
        for(Person person : people) {
            if(person.getName().equals(personName))
                return person;
        }
        return null;
    }

    public List<Person> getAllPeople() {
        return people;
    }

    public Map<String, Person> getAllPeopleMap() {
        return peopleMap;
    }

    public HttpStatus savePerson(Person person) {
        person.setLastTimeTracked(LocalTime.now());

        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(person.getName() + " has been added in the system and is now beeing tracked.");
        person.setTrackHistory(arrayList);

        people.add(person);
        peopleMap.put(person.getName().replaceAll(" ", ""), person);
        videoAnalysisService
            .indexFaceWithPersonName(getAWSImageFromBytes(person.getImages().get(0)),
                person.getName().replaceAll(" ", ""));

//        try {
//            BufferedImage bufferedImage = decodeToImage(base64Image);
//            File outputFile = new File("guest " + numberOfGuests + ".jpg");
//            ImageIO.write(bufferedImage, "jpg", outputFile);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        Event event = new Event();
        event.setTriggerPerson(person.getName());
        event.setEventTime(System.currentTimeMillis());
        event.setEventDescription("A new user has been added in the System");
        event.setEventName("New user added");
        event.setEventLocation("Endava Iasi");
        event.setEventType(NOTICE);
        eventService.triggerEvent(event, NEW_PERSON_REGISTERED);
        return HttpStatus.OK;
    }

//    public HttpStatus savePerson(Person person) {
//        people.add(person);
//        peopleMap.put(person.getName().replaceAll(" ", ""), person);
//        videoAnalysisService
//                .indexFaceWithPersonName(getAWSImageFromBytes(person.getImages().get(0)), person.getName().replaceAll(" ", ""));
//        return HttpStatus.OK;
//    }

    public HttpStatus addPhoto(String personName, byte[] personImage) {
        for(Person person : people) {
            if(person.getName().equals(personName)) {
                person.addImage(personImage);
                return HttpStatus.OK;
            }
        }
        return HttpStatus.NOT_FOUND;
    }

    private static BufferedImage decodeToImage(String base64String) throws IOException {
        BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(Base64.getDecoder().decode(base64String)));
        return bufferedImage;
    }
}
