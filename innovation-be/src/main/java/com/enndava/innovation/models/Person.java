package com.enndava.innovation.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Person {
    private String name;
    private List<byte[]> images;
    private int age = 25;
    private String department = "IT";
    private String role = "Junior Software Developer";
    private String[] roomsAccess;
    private ArrayList<String> trackHistory;
    private LocalTime lastTimeTracked;

    public void addImage(byte[] personImage) {
        images.add(personImage);
    }
}
