package com.amazonaws.kinesisvideo.demoapp;

import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.kinesisvideo.client.KinesisVideoClient;
import com.amazonaws.kinesisvideo.client.mediasource.CameraMediaSourceConfiguration;
import com.amazonaws.kinesisvideo.client.mediasource.MediaSource;
import com.amazonaws.kinesisvideo.common.exception.KinesisVideoException;
import com.amazonaws.kinesisvideo.java.client.KinesisVideoJavaClientFactory;
import com.amazonaws.kinesisvideo.mediasource.camera.CameraMediaSource;
import com.amazonaws.kinesisvideo.producer.StreamInfo;
import com.amazonaws.regions.Regions;
import com.github.sarxos.webcam.Webcam;


/**
 * Demo Java Producer.
 */
public final class DemoAppMain {
    private static final String STREAM_NAME = "AmazonRekognition-example-video";
    private static final int FPS_19 = 19;
    private static final int FPS_25 = 25;

    private DemoAppMain() {
        throw new UnsupportedOperationException();
    }

    public static void main(final String[] args) {
        try {
            final KinesisVideoClient kinesisVideoClient = KinesisVideoJavaClientFactory
                    .createKinesisVideoClient(
                            Regions.EU_WEST_1,
                            new DefaultAWSCredentialsProviderChain());

            final MediaSource cameraMediaSource = createCameraMediaSource();

            kinesisVideoClient.registerMediaSource(STREAM_NAME, cameraMediaSource);

            cameraMediaSource.start();

        } catch (final KinesisVideoException e) {
            throw new RuntimeException(e);
        }
    }

    private static MediaSource createCameraMediaSource() {

        Webcam webcam = Webcam.getDefault();

        byte[] cpd = { 0x01, 0x42, 0x00, 0x20, (byte) 0xff, (byte) 0xe1, 0x00, 0x23, 0x27, 0x42, 0x00, 0x20,
                (byte) 0x89, (byte) 0x8b, 0x60, 0x28, 0x02, (byte) 0xdd, (byte) 0x80, (byte) 0x9e, 0x00, 0x00,
                0x4e, 0x20, 0x00, 0x0f, 0x42, 0x41, (byte) 0xc0, (byte) 0xc0, 0x01, 0x77, 0x00, 0x00, 0x5d,
                (byte) 0xc1, 0x7b, (byte) 0xdf, 0x07, (byte) 0xc2, 0x21, 0x1b, (byte) 0x80, 0x01, 0x00, 0x04,
                0x28, (byte) 0xce, 0x1f, 0x20 };


        final CameraMediaSourceConfiguration configuration =
                new CameraMediaSourceConfiguration.Builder()
                        .withFrameRate(FPS_19)
                        .withRetentionPeriodInHours(1)
                        .withCameraId("/dev/video0")
                        .withIsEncoderHardwareAccelerated(true)
                        .withEncodingMimeType("video/h264")
                        .withNalAdaptationFlags(StreamInfo.NalAdaptationFlags.NAL_ADAPTATION_FLAG_NONE)
                        .withIsAbsoluteTimecode(false)
                        .withEncodingBitRate(200000)
                        .withHorizontalResolution(1280)
                        .withVerticalResolution(720)
                        .withCodecPrivateData(cpd)
                        .build();

        final CameraMediaSource mediaSource = new CameraMediaSource();
        mediaSource.setupWebCam(webcam);
        mediaSource.configure(configuration);
        return mediaSource;
    }

}
