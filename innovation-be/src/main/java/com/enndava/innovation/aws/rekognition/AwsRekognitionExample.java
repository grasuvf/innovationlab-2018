package com.enndava.innovation.aws.rekognition;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.rekognition.AmazonRekognition;
import com.amazonaws.services.rekognition.AmazonRekognitionClientBuilder;
import com.amazonaws.services.rekognition.model.*;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.List;

@Component
public class AwsRekognitionExample {

    private static final String[] HAZARDS = {"fire", "flame"};
    private static final String IMG_LABELS = "images/test.jpg";
    private static final String IMG_FACE_1 = "images/face1.jpg";
    private static final String IMG_FACE_2 = "images/face2.jpg";

//    @PostConstruct
    public void postC() {
        System.out.println("\n\n\nTEST FIREEEEEEEEE\n");

        try {
            main(getAwsImage2("P_20180520_075618_BF.jpg"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("\n\n\nTEST FIREEEEEEEEE\n");
    }

    public void main(Image image) throws Exception {

        AWSCredentials credentials;
        try {
            credentials = new ProfileCredentialsProvider("default").getCredentials();
        } catch (Exception e) {
            throw new AmazonClientException("Cannot load the credentials from the credential profiles file. "
                    + "Please make sure that your credentials file is at the correct "
                    + "location (/Users/userId/.aws/credentials), and is in a valid format.", e);
        }

        AmazonRekognition rekognitionClient = AmazonRekognitionClientBuilder
                .standard()
                .withRegion(Regions.EU_WEST_1)
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .build();

        AwsRekognitionExample awsRekognitionExample = new AwsRekognitionExample();

        awsRekognitionExample.detectLabels(rekognitionClient, image);
//        awsRekognitionExample.detectLabels(rekognitionClient, getAwsImage(IMG_LABELS));
//        awsRekognitionExample.compareFaces(rekognitionClient, getAwsImage(IMG_FACE_1), getAwsImage(IMG_FACE_2));
    }

    private void detectLabels(AmazonRekognition rekognitionClient, Image image) throws IOException, URISyntaxException {
        DetectLabelsRequest request = new DetectLabelsRequest()
                .withImage(image)
                .withMaxLabels(10)
                .withMinConfidence(75F);

        try {
            DetectLabelsResult result = rekognitionClient.detectLabels(request);
            List<Label> labels = result.getLabels();

            System.out.println("Detected labels:");
            for (Label label : labels) {
                System.out.println(hasHazard(label));
                System.out.println(label.getName() + ": " + label.getConfidence().toString());
            }
        } catch (AmazonRekognitionException e) {
            e.printStackTrace();
        }
    }

    private void compareFaces(AmazonRekognition rekognitionClient, Image source, Image target) throws IOException, URISyntaxException {

        CompareFacesRequest request = new CompareFacesRequest()
                .withSourceImage(source)
                .withTargetImage(target)
                .withSimilarityThreshold(75F);

        try {
            CompareFacesResult result = rekognitionClient.compareFaces(request);

            List<CompareFacesMatch> faceMatches = result.getFaceMatches();

            if (faceMatches.size() == 0){
                System.out.println("No match!");
            } else if (faceMatches.size() > 1) {
                System.out.println("Image contains more than one face!");
            } else {
                System.out.println("Match with " + faceMatches.get(0).getFace().getConfidence() + "% confidence");
            }

        } catch (AmazonRekognitionException e) {
            e.printStackTrace();
        }
    }

    private boolean hasHazard(Label label) {
        System.out.print(label + " Hazard: ");
        return Arrays.stream(HAZARDS).filter(h -> h.equals(label.getName().toLowerCase())).count() != 0;
    }

    public static Image getAwsImage(String imgPath) throws IOException, URISyntaxException {

        File image = new File(AwsRekognitionExample.class.getResource(imgPath).toURI());
        BufferedImage bImage = ImageIO.read(image);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ImageIO.write(bImage, "jpg", byteArrayOutputStream);
        return new Image().withBytes(ByteBuffer.wrap(byteArrayOutputStream.toByteArray()));
    }

    public static Image getAwsImage2(String imgPath) throws IOException, URISyntaxException {

        File image = new File(imgPath);
        BufferedImage bImage = ImageIO.read(image);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ImageIO.write(bImage, "jpg", byteArrayOutputStream);
        return new Image().withBytes(ByteBuffer.wrap(byteArrayOutputStream.toByteArray()));
    }
}
