package com.enndava.innovation.models;

import lombok.Data;

@Data
public class Authorization {
    private boolean authorized;
    private String personName;
}
