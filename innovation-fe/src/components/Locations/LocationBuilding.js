import React, {Component} from 'react';
import {Link} from "react-router-dom";
import Borders from './../Elements/Borders';

class LocationBuilding extends Component {
  constructor(props) {
    super(props);
  }

  eventInProgress(time) {
    let now = new Date(),
      notificationTime = new Date(time),
      differenceMinutes = Math.round((now - notificationTime) / 60000);

    return differenceMinutes <= 5; // 5 minutes
  }

  floorStatus(floor, notifications) {
    if(!floor.active) return 'unavailable';
    if(notifications.length === 0) return '';

    let thisNotification = notifications.filter(function(el) {
      return el.location.floor.toLowerCase() === floor.name.toLowerCase();
    });

    return thisNotification.length > 0 ? thisNotification[0].type : '';
  }

  render() {
    let allData = this.props.allData,
      thisCity = this.props.city,
      floors = allData.locations.filter(obj => {
        return obj.name.toLowerCase() === thisCity.toLowerCase()
      })[0].floors,
      notifications = allData.notifications.filter(obj => {
        return obj.location.city.toLowerCase() === thisCity.toLowerCase()
          && this.eventInProgress(obj.date)
      });

    return (
      <div className="location-building">
        <div className="map-title">
          <h2>Map</h2>
          <Borders top="false" right="false" bottom="true" left="false"/>
        </div>

        <div className="map-building">
          <div className="building-floors">
            <div className={"floors-map floors-map-"+floors.length}>
              {
                [...floors].reverse().map((floor, k) => {
                  return <div key={k} className={"floor "+this.floorStatus(floor, notifications)}>
                    <Link to={'/location/' + thisCity + '/' + floor.name}>
                      <span className="face back"></span>
                      <span className="face left"></span>
                      <span className="face right"></span>
                      <span className="face front"></span>
                      {
                        k === 0
                          ? <span className="face top"></span>
                          : null
                      }
                    </Link>
                  </div>
                })
              }
            </div>

            <div className="floors-list">
              <ul>
                {
                  [...floors].reverse().map((floor, k) => {
                    return <li key={k} className={this.floorStatus(floor, notifications)}>{floor.name}</li>
                  })
                }
              </ul>
            </div>
          </div>

          <div className="building-access">
            <ul>
              <li className="danger">Security Breach</li>
              <li className="warning">Warning</li>
              <li className="unavailable">No Access</li>
            </ul>
          </div>
        </div>

        <Borders top="true" right="true" bottom="true" left="true"/>
      </div>
    );
  }
}

export default LocationBuilding;
