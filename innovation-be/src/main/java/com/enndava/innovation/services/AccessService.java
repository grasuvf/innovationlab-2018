package com.enndava.innovation.services;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.rekognition.AmazonRekognition;
import com.amazonaws.services.rekognition.AmazonRekognitionClientBuilder;
import com.amazonaws.services.rekognition.model.*;
import com.enndava.innovation.constants.EventLevels;
import com.enndava.innovation.controllers.GetAccessResponse;
import com.enndava.innovation.models.Authorization;
import com.enndava.innovation.models.Event;
import com.enndava.innovation.models.Person;
import org.springframework.beans.factory.annotation.Autowired;

import java.nio.ByteBuffer;
import java.time.LocalTime;
import java.util.*;

import static com.enndava.innovation.constants.EventLevels.NOTICE;
import static com.enndava.innovation.constants.EventLevels.WARNING;
import static com.enndava.innovation.constants.EventTopics.SECURITY_BREACH_WARNING;

public class AccessService {

    @Autowired
    private PeopleService peopleService;

    @Autowired
    private EventService eventService;

    private AmazonRekognition rekognitionClient;

    public AccessService() {
        AWSCredentials credentials;
        try {
            credentials = new ProfileCredentialsProvider("default").getCredentials();
        } catch (Exception e) {
            throw new AmazonClientException("Cannot load the credentials from the credential profiles file. "
                    + "Please make sure that your credentials file is at the correct "
                    + "location (/Users/userId/.aws/credentials), and is in a valid format.", e);
        }

        rekognitionClient = AmazonRekognitionClientBuilder
                .standard()
                .withRegion(Regions.EU_WEST_1)
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .build();
    }

    public Authorization getAccessByBase64String(String base64StringImage, String roomName) {

        Image personImageAWS = this.getAWSImageFromBytes(Base64.getDecoder().decode(base64StringImage));

        for(Person person : peopleService.getAllPeople()) {
            if(this.compareFaces(personImageAWS, person.getImages())) {
                if(this.getAccess(person.getName(), roomName)) {
                    Event event = new Event();
                    event.setEventLocation(roomName);
                    event.setEventName("Successful Security Check");
                    event.setEventDescription("An authorized personel has requested access in the room: " + roomName);
                    event.setEventTime(System.currentTimeMillis());
                    event.setTriggerPerson(person.getName());
                    event.setEventType(NOTICE);

                    person.setLastTimeTracked(LocalTime.now());
                    if(person.getTrackHistory() != null) {
                        ArrayList<String> arrayList = person.getTrackHistory();
                        arrayList.add(person.getName() + " attempted to enter the " + roomName + " successfully");
                        person.setTrackHistory(arrayList);
                        person.addImage(Base64.getDecoder().decode(base64StringImage));
                    }

                    eventService.triggerEvent(event, SECURITY_BREACH_WARNING);

                    Authorization authorization = new Authorization();
                    authorization.setAuthorized(true);
                    authorization.setPersonName(person.getName());
                    return authorization;
                } else {
                    Event event = new Event();
                    event.setEventLocation(roomName);
                    event.setEventName("Unauthorized Security Check");
                    event.setEventDescription("An unauthorized personel has requested access in the room: " + roomName);
                    event.setEventTime(System.currentTimeMillis());
                    event.setTriggerPerson(person.getName());
                    event.setEventType(EventLevels.DANGER);

                    person.setLastTimeTracked(LocalTime.now());

                    ArrayList<String> arrayList;
                    if(person.getTrackHistory() != null) {
                        arrayList = person.getTrackHistory();
                    } else {
                        arrayList = new ArrayList<>();
                    }

                    arrayList.add(person.getName() + " attempted to enter the " + roomName + " while not having the required authorization");
                    person.setTrackHistory(arrayList);

                    eventService.triggerEvent(event, SECURITY_BREACH_WARNING);

                    Authorization authorization = new Authorization();
                    authorization.setAuthorized(false);
                    authorization.setPersonName(person.getName());
                    return authorization;
                }

            }
        }

        Event event = new Event();
        event.setEventLocation(roomName);
        event.setEventName("Unauthorized Security Check");
        event.setEventDescription("An unauthorized unknown personel has requested access in the room: " + roomName + ". This person is now beeing tracked.");
        event.setEventTime(System.currentTimeMillis());
        event.setTriggerPerson("Unknown person");
        event.setEventType(WARNING);
        eventService.triggerEvent(event, SECURITY_BREACH_WARNING);

//        peopleService.savePerson(base64StringImage);

        Authorization authorization = new Authorization();
        authorization.setPersonName("Unknown person");
        authorization.setAuthorized(false);
        return authorization;
    }

    public GetAccessResponse getAccess(byte[] personImage, String roomName) {

        Image personImageAWS = getAWSImageFromBytes(personImage);

        Map<String, Person> allPeopleMap = peopleService.getAllPeopleMap();

        SearchFacesByImageResult result = searchFace(personImageAWS);
        if (result.getFaceMatches().size() == 0) {
            Event event = new Event();
            event.setEventLocation(roomName);
            event.setEventName("Unauthorized Security Check");
            event.setEventDescription("An unknown personnel has requested access in the room: " + roomName);
            event.setEventTime(System.currentTimeMillis());
            event.setEventType(EventLevels.DANGER);
            eventService.triggerEvent(event, SECURITY_BREACH_WARNING);
            
            return new GetAccessResponse(null, false);
        }

        for (FaceMatch faceMatch : result.getFaceMatches()) {
            Person resPerson = allPeopleMap.get(faceMatch.getFace().getExternalImageId());
            if (resPerson != null) {
                List<String> pRooms = Arrays.asList(resPerson.getRoomsAccess());
                boolean hasAccess = pRooms.contains(roomName);
                if (!hasAccess) {
                    Event event = new Event();
                    event.setEventLocation(roomName);
                    event.setEventName("Unauthorized Security Check");
                    event.setEventDescription("An unauthorized personnel has requested access in the room: " + roomName);
                    event.setEventTime(System.currentTimeMillis());
                    event.setTriggerPerson(resPerson.getName());
                    event.setEventType(WARNING);
                    eventService.triggerEvent(event, SECURITY_BREACH_WARNING);
                }
                return new GetAccessResponse(resPerson.getName(), hasAccess);
            }
        }

        return new GetAccessResponse(null, false);
    }

    private SearchFacesByImageResult searchFace(Image image) {
        SearchFacesByImageRequest request = new SearchFacesByImageRequest()
                .withCollectionId(VideoAnalysisService.COLLECTION_ID)
                .withImage(image)
                .withFaceMatchThreshold(99f)
                .withMaxFaces(1);

        SearchFacesByImageResult result = rekognitionClient.searchFacesByImage(request);
        return result;
    }

    public Boolean getAccess(String personName, String roomName) {
        String allowedRooms[] = peopleService.getPerson(personName).getRoomsAccess();
        if(allowedRooms == null)
            return false;
        for(String room : allowedRooms) {
            if(room.equals(roomName))
                return true;
        }
        return false;
    }

    private boolean compareFaces(Image personImage, List<byte[]> images) {
        for(byte[] image : images) {
            Image imageAWS = getAWSImageFromBytes(image);
            if(compareFaces(personImage, imageAWS))
                return true;
        }

        return false;
    }

    private boolean compareFaces(Image first, Image second) {
        CompareFacesRequest request = new CompareFacesRequest()
                .withSourceImage(first)
                .withTargetImage(second)
                .withSimilarityThreshold(75F);

        try {
            CompareFacesResult result = rekognitionClient.compareFaces(request);
            List<CompareFacesMatch> faceMatches = result.getFaceMatches();

            if (faceMatches.size() == 0){
                System.out.println("No match!");
                return false;
            } else if (faceMatches.size() > 1) {
                System.out.println("Image contains more than one face!");
                return false;
            } else {
                System.out.println("Match with " + faceMatches.get(0).getFace().getConfidence() + "% confidence");

                if(faceMatches.get(0).getFace().getConfidence() > 65) {
                    return true;
                } else {
                    return false;
                }
            }

        } catch (AmazonRekognitionException e) {
            e.printStackTrace();
        }

        return false;
    }

    public static Image getAWSImageFromBytes(byte[] image) {
        ByteBuffer buffer = ByteBuffer.wrap(image);
        Image personImageAWS = new Image().withBytes(buffer);
        return personImageAWS;
    }
}
