package com.amazonaws.kinesisvideo.demoapp.setup;

import com.amazonaws.kinesisvideo.demoapp.service.VideoAnalysisService;

public class InitialSetup {

    private static final VideoAnalysisService videoAnalysisService = new VideoAnalysisService();

    // Run once
    public static void main(String[] args) {

        videoAnalysisService.createFacesCollection();
        videoAnalysisService.createStreamProcessor();
    }
}
