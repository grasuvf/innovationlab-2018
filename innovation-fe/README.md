# Innovation Front End - React

##To be updated.

For now, just run `npm start` from this directory. This will build the css files and open up a new browser window with the running app.

The project also has **hot reload**, so every time you save in the editor, the browser window will
refresh.

###Update
Since `node-sass` has been added, for **hot reload** to work properly, you need to also open a new terminal window and run `npm run sass:watch`
