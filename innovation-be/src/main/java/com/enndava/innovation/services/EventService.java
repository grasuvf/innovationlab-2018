package com.enndava.innovation.services;

import com.enndava.innovation.kafka.producer.KafkaEventProducer;
import com.enndava.innovation.models.Event;
import org.springframework.messaging.simp.SimpMessageSendingOperations;

//Use this when you need to trigger a hazardous event
public class EventService {

    private KafkaEventProducer kafkaEventProducer;
    private static SimpMessageSendingOperations messagingTemplate;

    public EventService(SimpMessageSendingOperations messagingTemplate) {
        this.messagingTemplate = messagingTemplate;
        kafkaEventProducer = new KafkaEventProducer(true, true);
    }

    public void triggerEvent(Event event, String eventString) {
        kafkaEventProducer.triggerEvent(eventString, event.getEventDescription());
        messagingTemplate.convertAndSend("/topic/" + eventString, event);
    }
}
