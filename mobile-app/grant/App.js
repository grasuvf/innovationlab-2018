/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native';

import { RNCamera } from 'react-native-camera';

import axios from 'axios';

const FullBackground = ({isLoading, authorized, personName}) => {
  const message = isLoading ? 'Authorizing...' : personName;
  const bgColor = isLoading ? '#f5fcff' : authorized ? 'green' : 'red';

  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: bgColor}}>
      <Text style={{fontSize: 32}}> {message} </Text>
    </View>
  );
}

export default class App extends Component {
  state = {
    isLoading: false,
    pictureTaken: false,
    authorized: false,
    personName: ''
  };

  render() {
    if (!this.state.pictureTaken) {
      return (
        <View style={styles.container}>
          <RNCamera
              ref={ref => {
                this.camera = ref;
              }}
              style = {styles.preview}
              type={RNCamera.Constants.Type.front}
              permissionDialogTitle={'Permission to use camera'}
              permissionDialogMessage={'We need your permission to use your camera phone'}
          />
          <View style={{flex: 0, flexDirection: 'row', justifyContent: 'center'}}>
          <TouchableOpacity
              onPress={this.takePicture.bind(this)}
              style = {styles.capture}
          >
              <Text style={{fontSize: 14}}> CAPTURE </Text>
          </TouchableOpacity>
          </View>
        </View>
      );
    }

    return <FullBackground 
      authorized={this.state.authorized}
      personName={this.state.personName}
      isLoading={this.state.isLoading} />;
  }

  takePicture = async function() {
    if (this.camera) {
      const options = {
        width: 1024,
        quality: 0.3, 
        base64: true 
      };
      this.camera.takePictureAsync(options).then(data => {
        this.setState({
          pictureTaken: true,
          isLoading: true
        });
        axios({
          method: 'POST',
          url:'http://192.168.1.112:8080/getAccessByImagePrivateRoom',
          //url:'http://192.168.1.178:4000/user/auth',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          data: data
        })
        .then(response => {
          this.setState({
            authorized: response.data.authorized,
            personName: response.data.personName,
            isLoading: false,
          });

          setTimeout(() => {
            this.setState({
              isLoading: false,
              pictureTaken: false,
              authorized: false,
              personName: ''
            })
          }, 5000);
        });
      }).catch(err => console.log(err));
    }
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black'
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20
  }
});
