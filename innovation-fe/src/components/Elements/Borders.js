import React, {Component} from 'react';

class Borders extends Component {
  render() {
    return (
      <div className="fancy-borders">
        {
          this.props.top === "true"
          ? <span className="fb-top"></span>
          : null
        }
        {
          this.props.right === "true"
          ? <span className="fb-right"></span>
          : null
        }
        {
          this.props.bottom === "true"
          ? <span className="fb-bottom"></span>
          : null
        }
        {
          this.props.left === "true"
          ? <span className="fb-left"></span>
          : null
        }
      </div>
    );
  }
}

export default Borders;
