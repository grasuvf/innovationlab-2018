package com.enndava.innovation.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import static com.enndava.innovation.constants.SecurityLevels.PUBLIC_AREA;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Room {
    private String roomName = "Room name";
    private String[] videoStreams;
    private String securityLevel = PUBLIC_AREA;
}
