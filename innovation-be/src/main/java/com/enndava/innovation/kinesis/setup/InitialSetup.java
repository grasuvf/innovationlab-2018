package com.enndava.innovation.kinesis.setup;


import com.enndava.innovation.kinesis.service.VideoAnalysisService;

import java.io.IOException;
import java.net.URISyntaxException;

public class InitialSetup {

    private static final VideoAnalysisService videoAnalysisService = new VideoAnalysisService();

    // Run once
    public static void main(String[] args) throws IOException, URISyntaxException {

//        videoAnalysisService.deleteStreamProcessor();
//        videoAnalysisService.createFacesCollection();
//        videoAnalysisService.createStreamProcessor();
//        videoAnalysisService.startStreamProcessor();
//        videoAnalysisService.describeStreamProcessor();
//        videoAnalysisService.indexFace(AwsRekognitionExample.getAwsImage("images/sample.jpg"));
//        videoAnalysisService.listFaces();
    }
}
