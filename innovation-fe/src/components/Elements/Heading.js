import React, {Component} from 'react';

class Heading extends Component {
  render() {
    return (
      <div className="heading">
        <h1>{this.props.title}</h1>
        {
          this.props.location
          ? <p>{this.props.location}</p>
          : <p> - </p>
        }
      </div>
    );
  }
}

export default Heading;
