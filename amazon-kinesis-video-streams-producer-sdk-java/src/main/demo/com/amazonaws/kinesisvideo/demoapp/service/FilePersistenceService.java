package com.amazonaws.kinesisvideo.demoapp.service;

import java.io.*;

public class FilePersistenceService {

    private static final String PERSISTENCE_FILE = "/resources/persistence/db.txt";

    public void write(String text) {
        try (PrintWriter writer = new PrintWriter(PERSISTENCE_FILE, "UTF-8")) {
            writer.println(text);
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public String readCollectionId() {
        try (BufferedReader br = new BufferedReader(new FileReader(PERSISTENCE_FILE))) {
            String line = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String read() {
        try (BufferedReader br = new BufferedReader(new FileReader(PERSISTENCE_FILE))) {
            return br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
