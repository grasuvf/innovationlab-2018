import React, {Component} from 'react';
import Heading from './Elements/Heading';
import Notifications from './Notifications/Notifications';

class Home extends Component {
  render() {
    let allData = this.props.allData;
    
    return (
      <div className="grid-container">
        <div className="grid-row">
          <div className="grid-col-10">
            <Heading title={'General Access Admin'} />
          </div>
        </div>

        <div className="grid-row">
          <div className="grid-col-5">
            <Notifications type="danger" max="5" allData={allData}/>
          </div>

          <div className="grid-col-5">
            <Notifications type="warning" max="5" allData={allData} />
          </div>
        </div>
      </div>
    );
  }
}

export default Home;
